Relay Debounce Counter

This is a timer controling a relay. It features a buzzer for audible alert, a led for visible status and a button to provide extra time.

I am using it to make sure that I do not leave my solder iron plugged. I know that it was easier and cheaper to buy a simple AC timer. But I live in a third world country, so the only options that I found were expensive industrial timers, that need to connect to industrial timer and some fancy support and enclosing.

The idea is simple. It uses the internal counter of the arduino with function millis. It activate the realy on power on. When millis reach one hour it will deactivate the relay. 5 minutes before the hour it will buzz to let you know that time is about to expire. It will blink the led as reminder. After the hour it will deactivate the relay cutting the energy to the load. At any given time, you can push the button for 10 extra minutes. Mind that if the hour has already pass, you may have to press several times the button. At that point it will be easier to press the reset buton on the arduino.

It do not have active led, as the relay module has already one led for power and onother led for active.


Neville A. Cross
yn1v@taygon.com
2018-08-10

GPL.
Use it and modify to suit your needs.
Provide atribution if you share it.