/*
 * Neville A. Cross yn1v@taygon.com 2018-08-10 GPL
 * Please use, if you share please provide atribution.
 * 
 * This codes re-use code from arduino examples:
 * - BlikWhioutDelay
 * - Debounce
 * 
 * On power ON, it activate the relay to pass current for one hour.
 * 5 minutes before the hour it beeps and start blinking a led
 * On the hour it deactivate the relay to avoid curent flowing.
 * The led will remain on, showing tha time has expired.
 * At any time, you can get 10 extra minutes by pressing the button.
 * (Mind that if the hour has expired long ago, it will be easier to reset the arduino)
 * 
 */
 
const int ledPin = 8;
const int relayPin = 4;
const int buzzPin = 6;
const int buttonPin = 2;
int ledState = LOW;
int lastButtonState = LOW;
int reading;
int buttonState;
unsigned long timeOff = 3600000; // 1 hour
unsigned long snooze = 600000; //10  minutes
unsigned long alert = 300000; // 5 minutes
unsigned long interval = 500;
unsigned long debounceDelay = 50;  
unsigned long timeAlert = 0;
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
unsigned long lastDebounceTime = 0;

void setup() {
     pinMode(buttonPin, INPUT);
     pinMode(relayPin, OUTPUT);
     pinMode(buzzPin, OUTPUT);
     pinMode(ledPin, OUTPUT);
     digitalWrite(relayPin, LOW);
     // Serial.begin(9600);
}

void loop() {
  /*
  Serial.print("Millis: ");
  Serial.print(millis());
  Serial.print("\ttimeOff: ");
  Serial.print(timeOff);
  Serial.print("\t timeAlert: ");
  Serial.println(timeAlert);
  */

  // Begin of debouncing section
  timeAlert = timeOff - alert;
  reading = digitalRead(buttonPin);
   if (reading != lastButtonState) {
      lastDebounceTime = millis();
      }
   if ((millis() - lastDebounceTime) > debounceDelay) {
      if (reading != buttonState) {
         buttonState = reading;
         if (buttonState == HIGH) {
            timeOff = timeOff + snooze;
            ledState=LOW;
            }  
         }
       }

  lastButtonState = reading;
  // End of debouncin section
  
  delay(100);
  
  if (millis() > timeOff){
    digitalWrite(relayPin, HIGH);
    digitalWrite(ledPin, HIGH);
    tone(buzzPin, 500);
    delay (250);
    noTone(buzzPin);
    tone(buzzPin, 500);
    delay (250);
    noTone(buzzPin);
    }

  if (millis() > (timeAlert) && timeOff> millis()){
     if (millis() < timeAlert + 500){
        tone(buzzPin, 500);
        delay (500);
        noTone(buzzPin);
       }
    
      else{
         //Beging of blink withou delay
         currentMillis = millis();
         if (currentMillis - previousMillis >= interval){
            previousMillis = currentMillis;
            if (ledState == LOW){
               ledState = HIGH;
             }
            else {
               ledState = LOW;
             }
        
           }
        
         digitalWrite(ledPin,ledState);
         //End of blink without dealy
         }
    
   }


}
