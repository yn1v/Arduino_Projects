/*
Based on Serial Event example included in Arduino
Modified to meet the requirement of
Peer-graded Assignment: Module 4 Peer Review

https://gitlab.com/yn1v/Arduino_Projects
*/

String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

int led = LED_BUILTIN; // variable for the output pin

void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  pinMode(led, OUTPUT);
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    Serial.println(inputString);
    // checking the content of the string
    if (inputString == "1"){
      Serial.println("Turning Built in led ON");
      digitalWrite(led, HIGH); 
    }
    else if (inputString == "0"){
      Serial.println("Turning Built in led OFF");
      digitalWrite(led, LOW); 
    }
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

/*
 This code read the serial communication
 Changed from original code to strip new line
 */
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar != '\n') {
      inputString += inChar;
    }   
     else {
      stringComplete = true;
    }
  }
}
