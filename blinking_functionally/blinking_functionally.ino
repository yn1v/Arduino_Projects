/* 
Blink built in LED
5 times 1 sec period (ON 1/2 sec - OFF 1/2)
5 times 4 sec period (ON 2 sec - OFF 2)

https://gitlab.com/yn1v/Arduino_Projects
*/

int my_led = LED_BUILTIN;
int counter;

void setup(){
  pinMode(my_led, OUTPUT);
}

void flash(int t, int p){
  // t => how many times it will flash
  // p => period for flashing in seconds
  int pause;
  pause = p * 1000 / 2;
  for (counter = 0; counter < t; ++counter){
    digitalWrite(my_led, HIGH);
    delay(pause);
    digitalWrite(my_led, LOW);
    delay(pause);
    }
}

void loop(){
  flash(5, 1);
  flash(5, 4);
}
